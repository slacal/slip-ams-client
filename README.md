# SLIP AMS Client #

This project contains a sample client project for SLIP AMS.

### What is this repository for? ###

* This repository demonstrates connecting to the [Surplus Line of California](https://www.slacal.com/) SLIP AMS API for the purpose of integrating an Agent Management System with SLIP AMS API.

### Projects Overview ###
#### SLIP-AMS-Client ####

* This project is the main entry point for the whole solution.

* The client includes options to test the following:
    1. Authenticate
    2. Get Submission
    3. Get Submission Transactions
    4. Create Submission

#### SLIP-AMS-Core ####

* This project contains the main domain models. 

#### SLIP-AMS-Infrastructure ####

* This project contains all the services and business logic after we get the data from REST endpoints.

### Get started

Prerequisites

* .NET Core 3.1 or higher

Follow these steps to start using the SLIP AMS Client.

1. Download and unzip the repository

2. Update the `AuthenticationCredentials` settings in *SLACAL.SLIP.AMS.Client\appsettings.json*

    See the [Getting API Access](https://bitbucket.org/slacal/slip-ams-developer/src/master/get-access.md) documentation page for instructions for acquiring an API Token.

3. Note that these sample files are populated with example data. When adapting AMS files for your use, make sure you enter the actual broker # into the files.  

4. Run the client project.

    ``` PS
    SLACAL.SLIP.AMS.Client>dotnet run
     ```

5. The client project also exposes a CLI interface. The options can be seen as follows:

    ``` PS
    SLACAL.SLIP.AMS.Client>dotnet run --ams-help
     ```

### References ###

* [SLIP AMS Info Page](http://www.slacal.com/slip-ams)
* [SLIP AMS Developer Documentation](https://bitbucket.org/slacal/slip-ams-developer)
* [API Documentation](https://slip-api.slacal.com/docs/index)