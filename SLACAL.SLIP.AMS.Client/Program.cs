﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using SLACAL.SLIP.AMS.Core.Interfaces;
using SLACAL.SLIP.AMS.Infrastructure;
using AllowedValuesAttribute = McMaster.Extensions.CommandLineUtils.AllowedValuesAttribute;

namespace SLACAL.SLIP.AMS.Client
{
    [Command(Name = "SLACAL.SLIP.AMS.Client", Description = "This is an example console application to use and test SLIP AMS API REST endpoints.")]
    [HelpOption("--ams-help")]
    public class Program
    {
        [Option(Description = "Endpoint to be tested", ShortName = "e")]
        [AllowedValues(TestActions.Authenticate, TestActions.CreateSubmission, TestActions.GetSubmission, TestActions.GetTransactions, IgnoreCase = true)] // IgnoreCase allows case-insensitive comparison
        public string Endpoint { get; set; }

        [Option(Description = "Submission number. Required for get-submission and get-transactions.", ShortName = "n")]
        public string SubmissionNumber { get; set; }

        [Option(Description = "Sample JSON file. Required for create-submission.", ShortName = "s")]
        public string SampleFile { get; set; }
        
        public static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += Program.CurrentDomainUnhandledException;

            TaskScheduler.UnobservedTaskException += Program.TaskSchedulerUnobservedTaskException;

            var configBuilder = new ConfigurationBuilder();

            var config = Program.BuildConfig(configBuilder);

            Program.ConfigureLogging(config);

            Program.ConfigureDependencyInjection();

            CommandLineApplication.Execute<Program>(args);
        }

        private static IHost DependencyInjectionHost { get; set; }

        private static IConfiguration BuildConfig(IConfigurationBuilder builder)
        {
            return builder.SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
            .AddEnvironmentVariables()
            .Build();
        }

        private static void ConfigureDependencyInjection()
        {
            Program.DependencyInjectionHost = Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) =>
                {
                    services.AddTransient<IAuthenticateService, AuthenticateService>();
                    services.AddTransient<ISubmissionService, SubmissionService>();
                })
                .UseSerilog()
                .Build();
        }

        private static void ConfigureLogging(IConfiguration config)
        {
            Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(config)
            .Enrich.FromLogContext()
            .WriteTo.Console()
            .CreateLogger();
        }

        public static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.ExceptionObject is Exception ex)
            {
                Log.Fatal(ex, "Unhandled exception.");
            }
            else
            {
                Log.Fatal("Unhandled exception without exception object.");
            }
        }

        public static void TaskSchedulerUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            if (e.Exception != null)
            {
                Log.Error(e.Exception, "Unobserved task exception.");
            }
            else
            {
                Log.Error("Unobserved task exception without exception object.");
            }
        }

        private async Task<string> GetJwtToken()
        {
            var authenticateService = ActivatorUtilities.CreateInstance<AuthenticateService>(Program.DependencyInjectionHost.Services);
            return await authenticateService.Authenticate();
        }

        private async Task OnExecute()
        {
            Log.Logger.Information("Application starting");

            int selectedActionIndex;

            if (Endpoint != null)
            {
                selectedActionIndex = Array.IndexOf(TestActions.All, Endpoint);
            }
            else
            {
                selectedActionIndex = GetSelectedInt("Which end point would you like to test? (type index)", TestActions.All);
            }

            var theAction = TestActions.All[selectedActionIndex];

            if (theAction == TestActions.Authenticate)
            {
                Log.Logger.Information("Testing authentication");

                var jwtToken = await GetJwtToken();

                if (!string.IsNullOrEmpty(jwtToken))
                {
                    Log.Logger.Information("Authentication successful");
                }
                else
                {
                    Log.Logger.Error("Authentication failed");
                }
            }
            
            if (theAction == TestActions.GetSubmission)
            {
                if (Endpoint == null)
                {
                    Console.WriteLine("Submission Number:");
                    SubmissionNumber = Console.ReadLine();
                }

                Log.Logger.Information($"Testing get submission for submission number {SubmissionNumber}");

                var jwtToken = await GetJwtToken();

                if (!string.IsNullOrEmpty(jwtToken))
                {
                    Log.Logger.Information("Authentication successful");
                    var submissionService = ActivatorUtilities.CreateInstance<SubmissionService>(Program.DependencyInjectionHost.Services);
                    var submissionStatus = await submissionService.GetSubmission(jwtToken, SubmissionNumber);

                    if (submissionStatus == null)
                    {
                        Log.Logger.Warning($"Get Submission failed for Submission Number {SubmissionNumber}");
                    }
                    else
                    {
                        Log.Logger.Information($"Get Submission successful for Submission Number {SubmissionNumber}");
                    }
                }
                else
                {
                    Log.Logger.Error("Authentication failed");
                }
            }
            
            if (theAction == TestActions.GetTransactions)
            {
                if (Endpoint == null)
                {
                    Console.WriteLine("Submission Number:");
                    SubmissionNumber = Console.ReadLine();
                }

                Log.Logger.Information($"Testing get transactions for submission number {SubmissionNumber}");

                var jwtToken = await GetJwtToken();

                if (!string.IsNullOrEmpty(jwtToken))
                {
                    Log.Logger.Information("Authentication successful");
                    var submissionService = ActivatorUtilities.CreateInstance<SubmissionService>(Program.DependencyInjectionHost.Services);
                    var submissionTransactions = await submissionService.GetTransactions(jwtToken, SubmissionNumber);

                    if (submissionTransactions == null)
                    {
                        Log.Logger.Warning($"Get Transactions failed for Submission Number {SubmissionNumber}");
                    }
                    else
                    {
                        Log.Logger.Information($"Get Transactions successful for Submission Number {SubmissionNumber}");
                    }
                }
                else
                {
                    Log.Logger.Error("Authentication failed");
                }
            }
            
            if (theAction == TestActions.CreateSubmission)
            {
                Log.Logger.Information("Testing create submission");

                var files = Directory.GetFiles("samples", "*.json");
                int selectedIndex;

                if (Endpoint == null)
                {
                    selectedIndex = GetSelectedInt("Choose test file:", files);
                }
                else
                {
                    SampleFile = $"samples\\{SampleFile}";
                    selectedIndex = Array.IndexOf(files, SampleFile);
                    if (selectedIndex == -1)
                    {
                        Log.Logger.Error($"File {SampleFile} not found in samples directory");
                        return;
                    }
                }

                var jsonFile = files[selectedIndex];
                var zipFile = $"{jsonFile.Split('.')[0]}.zip";

                Log.Logger.Information("create-submission with zip file location: " + zipFile);

                Log.Logger.Information("Authenticate the Broker");

                var jwtToken = await GetJwtToken();

                if (!string.IsNullOrEmpty(jwtToken))
                {   
                    Log.Logger.Information("Authentication successful");

                    var submissionService = ActivatorUtilities.CreateInstance<SubmissionService>(Program.DependencyInjectionHost.Services);
                    var newSubmissionNumber = await submissionService.CreateNewSubmission(jwtToken, jsonFile, zipFile);

                    if (!string.IsNullOrEmpty(newSubmissionNumber))
                    {
                        Log.Logger.Information($"Submission created with Submission Number {newSubmissionNumber}");
                    }
                    else
                    {
                        Log.Logger.Warning($"Submission creation failed");
                    }
                }
                else
                {
                    Log.Logger.Error("Authentication failed");
                }
            }

            if (theAction == TestActions.GetTransactionTags)
            {
                Console.WriteLine("Submission Number:");
                var submissionNumber = Console.ReadLine();

                Log.Logger.Information($"Testing get transaction tags for submission number {submissionNumber}");

                var jwtToken = await GetJwtToken();

                if (!string.IsNullOrEmpty(jwtToken))
                {
                    Log.Logger.Information("Authentication successful");
                    var submissionService = ActivatorUtilities.CreateInstance<SubmissionService>(Program.DependencyInjectionHost.Services);
                    var submissionTransactions = await submissionService.GetTransactionTags(jwtToken, submissionNumber);

                    if (submissionTransactions == null)
                    {
                        Log.Logger.Warning($"Get Transaction tags failed for Submission Number {submissionNumber}");
                    }
                    else
                    {
                        Log.Logger.Information($"Get Transaction tags successful for Submission Number {submissionNumber}");
                    }
                }
                else
                {
                    Log.Logger.Error("Authentication failed");
                }
            }
        }

        
        private ValidationResult OnValidate()
        {
            if ((Endpoint == TestActions.GetSubmission || Endpoint == TestActions.GetTransactions) && SubmissionNumber == null)
            {
                return new ValidationResult($"SubmissionNumber is required for {Endpoint} endpoint.");
            }

            if (Endpoint == TestActions.CreateSubmission && string.IsNullOrEmpty(SampleFile))
            {
                return new ValidationResult("SampleFile is required for CreateSubmission endpoint.");
            }

            return ValidationResult.Success;
        }

        private int GetSelectedInt(string prompt, string[] options)
        {
            while (true)
            {
                Console.WriteLine(prompt);
                for (var index = 0; index < options.Length; index++)
                {
                    Console.WriteLine($"{index + 1} - {options[index]}");
                }

                var input = Console.ReadLine();
                var isInt = int.TryParse(input, out int parsedInt);

                if (!isInt)
                {
                    Console.WriteLine("Input needs to be a number");
                    continue;
                }

                if (parsedInt > options.Length)
                {
                    Console.WriteLine("Input must be one of the printed numbers");
                    continue;
                }

                return parsedInt - 1;
            }
        }
    }
}