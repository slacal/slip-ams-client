﻿namespace SLACAL.SLIP.AMS.Client
{
    public static class TestActions
    {
        public const string Authenticate = "authenticate";
        public const string GetSubmission = "get-submission";
        public const string GetTransactions = "get-transactions";
        public const string CreateSubmission = "create-submission";
        public const string GetTransactionTags = "get-transactions-tags";

        public static string[] All = new string[]
        {
            Authenticate,
            GetSubmission,
            GetTransactions,
            CreateSubmission,
            GetTransactionTags
        };
    }
}
