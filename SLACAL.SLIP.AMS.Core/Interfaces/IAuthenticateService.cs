﻿using System.Threading.Tasks;

namespace SLACAL.SLIP.AMS.Core.Interfaces
{
    public interface IAuthenticateService
    {
        Task<string> Authenticate();
    }
}