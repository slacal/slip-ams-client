﻿using System.Threading.Tasks;
using SLACAL.SLIP.AMS.Core.Domain;

namespace SLACAL.SLIP.AMS.Core.Interfaces
{
    public interface ISubmissionService
    {
        Task<GetSubmissionResult> GetSubmission(string jwtToken, string submissionNumber);

        Task<SubmissionTransactions> GetTransactions(string jwtToken, string submissionNumber);

        Task<string> CreateNewSubmission(string jwtToken, string jsonFilePath, string zipFilePath);

        Task<SubmissionTransactionTags> GetTransactionTags(string jwtToken, string submissionNumber);
    }
}
