﻿using System;
using System.Collections.Generic;

namespace SLACAL.SLIP.AMS.Core.Domain
{
    public class GetSubmissionResult
    {
        /// <summary>
        /// Submission Number assigned to uniquely identify the batch submission
        /// </summary>
        public string SubmissionNumber;

        /// <summary>
        /// The status of the batch submission.
        /// Processing, Failed, Pending User Action, Pending SLA Review, Closed, or Returned
        /// </summary>        
        public StatusTypeEnum? Status;

        /// <summary>
        /// List of SubmissionError
        /// </summary>
        public List<SubmissionError> Errors;

        /// <summary>
        /// List of Notification
        /// </summary>
        public List<Notification> Notifications;
    }

    public enum StatusTypeEnum
    {
        /// <summary>
        /// Batch Status is "Uploaded"
        /// </summary>
        Uploaded = 1,

        /// <summary>
        /// Batch Status is "Invalid Upload"
        /// </summary>
        InvalidUpload = 2,

        /// <summary>
        /// Batch Status is "Pending User Action"
        /// </summary>
        PendingUserAction = 3,

        /// <summary>
        /// External Batch Status is "Pending SLA Review"
        /// </summary>
        PendingSLAReview = 4,

        /// <summary>
        /// External Batch Status is "Closed by the SLA"
        /// </summary>
        Closed = 5,

        /// <summary>
        /// External Batch Status is "Returned by the SLA"
        /// </summary>
        Returned = 6
    }

    /// <summary>
    /// Submission Error detail for each element in a batch
    /// </summary>
    public class SubmissionError
    {
        /// <summary>
        /// The custom element identifier specified by the AMS in the XML data file previously submitted
        /// </summary>
        public string ElementId;

        /// <summary>
        /// Identifies the type of element within the submitted data that the document is to be associated with
        /// </summary>
        public ElementTypeEnum ElementType;

        /// <summary>
        /// Error message if submission has any errors
        /// </summary>
        public string ErrorMessage;
    }

    public enum ElementTypeEnum
    {
        /// <summary>
        /// Batch Element Type is Policy
        /// </summary>
        Policy,

        /// <summary>
        /// Batch Element Type is Transaction
        /// </summary>
        Transaction
    }

    public class Notification
    {
        /// <summary>
        /// Notification Type defined in the Notification Type
        /// Alert, TIQ, or TAG
        /// </summary>
        public NotificationTypeEnum Type;

        /// <summary>
        /// The custom element identifier specified by the AMS in the XML data file previously submitted
        /// </summary>
        public string ElementId;

        /// <summary>
        /// Identifies the type of element within the submitted data that the document is to be associated with
        /// </summary>
        public ElementTypeEnum ElementType;

        /// <summary>
        /// Message for the specified notification type
        /// </summary>
        public string Message;
    }

    public enum NotificationTypeEnum
    {
        /// <summary>
        /// Notification Type is Alert
        /// </summary>
        Alert,

        /// <summary>
        /// Notification Type is TIQ
        /// </summary>
        TIQ,

        /// <summary>
        /// Notification Type is TAG
        /// </summary>
        TAG
    }
}
