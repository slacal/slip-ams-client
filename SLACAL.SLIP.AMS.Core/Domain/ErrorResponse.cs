﻿using System.Collections.Generic;

namespace SLACAL.SLIP.AMS.Core.Domain
{
    public class ErrorResponse
    {
        /// <summary>
        /// List of errors describing why the request for transactions is invalid.
        /// </summary>
        public List<string> Errors { get; set; }
    }
}
