﻿using System;
using System.Collections.Generic;

namespace SLACAL.SLIP.AMS.Core.Domain
{
    public class SubmissionTransactions
    {
        /// <summary>
        /// Submission Number assigned to uniquely identify the batch submission
        /// </summary>
        public string SubmissionNumber;

        /// <summary>
        /// List of Transaction Details
        /// </summary>
        public List<TransactionDetail> Transactions;
    }

    public class TransactionDetail
    {
        //// <summary>
        /// Transaction Id / Agent Transaction Id
        /// </summary>
        public int TransactionID;

        /// <summary>
        /// Policy Number for Transsaction
        /// </summary>
        public string PolicyNumber;

        /// <summary>
        /// Identifies the type of Transaction ie. New, Endorsement, Audit, Cancellation, Renewal, Extension or Offset
        /// </summary>
        public string TransactionType;

        /// <summary>
        /// Premium for the Transaction
        /// </summary>
        public decimal? Premium;

        /// <summary>
        /// Taxable Fees for the Transaction
        /// </summary>
        public decimal? TaxableFees;

        /// <summary>
        /// Name of Insured 
        /// </summary>
        public string InsuredName;

        /// <summary>
        /// Endorsement Number
        /// </summary>
        public string EndorsementNumber;

        /// <summary>
        ///  Effective Date from which Transaction / Policy is valid
        /// </summary>
        public DateTime? EffectiveDate;

        /// <summary>
        /// The status of the transaction.
        /// Pending, Registered, or Returned
        /// </summary>
        public TransactionStatusTypeEnum? Status;
    }

    public enum TransactionStatusTypeEnum
    {
        /// <summary>
        /// Transaction Status is "Pending"
        /// </summary>
        Pending = 'P',

        /// <summary>
        /// Transaction Status is "Registered"
        /// </summary>
        Registered = 'R',

        /// <summary>
        /// Transaction Status is "Returned"
        /// </summary>
        Returned = 'T'
    }
}
