using System;
using System.Collections.Generic;

namespace SLACAL.SLIP.AMS.Core.Domain
{
    public class SubmissionTransactionTags
    {
        /// <summary>
        /// Submission Number assigned to uniquely identify the batch submission
        /// </summary>
        public string SubmissionNumber;

        /// <summary>
        /// List of Transaction Details
        /// </summary>
        public List<TransactionDetailTags> Transactions;
    }

    public class TransactionDetailTags : TransactionDetail
    {
        
        /// <summary>
        /// List of Tag Details
        /// </summary>
        public List<TagDetail> TagDetails;
    }

    public class TagDetail
    {
        /// <summary>
        /// Tag ID
        /// </summary>
        public string TagNumber;

        /// <summary>
        /// Tag code
        /// </summary>
        public string TagCode;

        /// <summary>
        /// Tag type
        /// </summary>
        public string TagType;

        /// <summary>
        /// Category
        /// </summary>
        public string Category;

        /// <summary>
        /// Tag group
        /// </summary>
        public string TagGroup;

        /// <summary>
        /// Tag text
        /// </summary>
        public string TagText;

        /// <summary>
        /// Previous replies
        /// </summary>
        public string PreviousReplies;

        /// <summary>
        /// Assigned date
        /// </summary>
        public DateTime? AssignedDate;

        /// <summary>
        /// Due date
        /// </summary>
        public DateTime? DueDate;

        /// <summary>
        /// Status code
        /// </summary>
        public string StatusCode;

        /// <summary>
        /// Status
        /// </summary>
        public string Status;

    }
}
