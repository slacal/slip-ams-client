﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SLACAL.SLIP.AMS.Core.Domain;
using SLACAL.SLIP.AMS.Core.Interfaces;

namespace SLACAL.SLIP.AMS.Infrastructure
{
    public class SubmissionService : ISubmissionService
    {
        public SubmissionService(IConfiguration config, ILogger<SubmissionService> logger)
        {
            this.Config = config;
            this.Logger = logger;
        }

        private IConfiguration Config { get; }

        private ILogger<SubmissionService> Logger { get; }

        public async Task<GetSubmissionResult> GetSubmission(string jwtToken, string submissionNumber)
        {
            this.Logger.LogInformation("GetSubmission() called in SubmissionService for submission Number: " + submissionNumber);

            var baseUrl = this.Config.GetValue<string>("SLIPAMSAPIAddress");
            var getSubmissionUrl = $"{baseUrl}api/submissions/{submissionNumber}";

            using (HttpClient client = new HttpClient())
            {
                // Add JWT Token received after successful authentication.
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                this.Logger.LogInformation("Making an API call to GetSubmission with url: " + getSubmissionUrl);

                //Setting up the response...         
                var response = await client.GetAsync(getSubmissionUrl);

                this.Logger.LogInformation($"Response Status Code is {response.StatusCode}");

                if (response.IsSuccessStatusCode)
                {
                    this.Logger.LogInformation("Get Submission successfully called");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    this.Logger.LogInformation($"Response content is {responseContent}");

                    var submissionStatus = JsonConvert.DeserializeObject<GetSubmissionResult>(responseContent);

                    this.Logger.LogInformation($"Submission number: {submissionStatus.SubmissionNumber}");
                    this.Logger.LogInformation($"Submission status: {submissionStatus.Status}");

                    if (submissionStatus.Errors != null)
                    {
                        this.Logger.LogInformation($"Number of Submission Errors: {(submissionStatus.Errors != null ? submissionStatus.Errors.Count : 0)}");
                        foreach (var error in submissionStatus.Errors)
                        {
                            this.Logger.LogInformation("******************************");
                            this.Logger.LogInformation($"Submission Error Element Id: {error.ElementId}");
                            this.Logger.LogInformation($"Submission Error Element Type: {error.ElementType}");
                            this.Logger.LogInformation($"Submission Error Error Message: {error.ErrorMessage}");
                            this.Logger.LogInformation("******************************");
                        }
                    }
                    else
                    {
                        this.Logger.LogInformation("Number of Submission Errors: 0");
                    }

                    if (submissionStatus.Notifications != null)
                    {
                        this.Logger.LogInformation($"Number of Notifications: {submissionStatus.Notifications.Count}");

                        foreach (var notification in submissionStatus.Notifications)
                        {
                            this.Logger.LogInformation("******************************");
                            this.Logger.LogInformation($"Submission Notification Type: {notification.Type}");
                            this.Logger.LogInformation($"Submission Notification Element Id: {notification.ElementId}");
                            this.Logger.LogInformation($"Submission Notification Element Type: {notification.ElementType}");
                            this.Logger.LogInformation($"Submission Notification Message: {notification.Message}");
                            this.Logger.LogInformation("******************************");
                        }
                    }
                    else
                    {
                        this.Logger.LogInformation("Number of Notifications: 0");
                    }

                    return submissionStatus;
                }
                else if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    this.Logger.LogWarning("Get submission error returned Unauthorized status ");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(responseContent);

                    foreach (var error in errorResponse.Errors)
                    {
                        this.Logger.LogWarning($"Get submission error: {error}");
                    }

                    return null;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    this.Logger.LogWarning("Get submission error returned InternalServerError status  ");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    var error = JsonConvert.DeserializeObject<dynamic>(responseContent);

                    this.Logger.LogWarning($"{error}");

                    return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task<SubmissionTransactions> GetTransactions(string jwtToken, string submissionNumber)
        {
            this.Logger.LogInformation("GetTransactions() called in SubmissionService for submission Number: " + submissionNumber);

            var baseUrl = this.Config.GetValue<string>("SLIPAMSAPIAddress");
            var getTransactionUrl = $"{baseUrl}api/submissions/{submissionNumber}/transactions";

            using (HttpClient client = new HttpClient())
            using (var formData = new MultipartFormDataContent())
            {
                // Add all required values to be sent to API.
                formData.Add(new StringContent(submissionNumber), "submissionNumber");

                // Add JWT Token received after successful authentication.
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                
                this.Logger.LogInformation("Making an API call to GetTransactions with url: " + getTransactionUrl);

                //Setting up the response...
                var response = client.GetAsync(getTransactionUrl).Result;

                this.Logger.LogInformation($"Response Status Code is {response.StatusCode.ToString()}");

                if (response.IsSuccessStatusCode)
                {
                    this.Logger.LogInformation("Get Transations successfully called");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    this.Logger.LogInformation($"Response content is {responseContent}");

                    var submissionTransactions = JsonConvert.DeserializeObject<SubmissionTransactions>(responseContent);

                    this.Logger.LogInformation($"Submission number: {submissionTransactions.SubmissionNumber}");
                    this.Logger.LogInformation($"Number of Transactions: {submissionTransactions.Transactions.Count}");

                    var transactions = new List<TransactionDetail>();
                    transactions = submissionTransactions.Transactions;
                    
                    foreach (var transaction in transactions)
                    {
                        this.Logger.LogInformation("******************************");
                        this.Logger.LogInformation($"Transaction ID: {transaction.TransactionID}");
                        this.Logger.LogInformation($"Policy Number: {transaction.PolicyNumber}");
                        this.Logger.LogInformation($"Transaction Type: {transaction.TransactionType}");
                        this.Logger.LogInformation($"Premium: ${transaction.Premium}");
                        this.Logger.LogInformation($"Taxable Fees: ${transaction.TaxableFees}");
                        this.Logger.LogInformation($"Insured Name: {transaction.InsuredName}");
                        this.Logger.LogInformation($"Endorsement Number: {transaction.EndorsementNumber}");
                        this.Logger.LogInformation($"Effective Date: {String.Format("{0:MM/dd/yyyy}", transaction.EffectiveDate)}");
                        this.Logger.LogInformation($"Status: {transaction.Status}");
                        this.Logger.LogInformation("******************************");
                    }

                    return submissionTransactions;
                }
                else if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    this.Logger.LogWarning("Get Transactions error returned Unauthorized status ");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(responseContent);

                    foreach (var error in errorResponse.Errors)
                    {
                        this.Logger.LogWarning($"Get Transactions error: {error}");
                    }

                    return null;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    this.Logger.LogWarning("Get Transactions error returned InternalServerError status  ");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    var error = JsonConvert.DeserializeObject<dynamic>(responseContent);

                    this.Logger.LogWarning($"{error}");

                    return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task<SubmissionTransactionTags> GetTransactionTags(string jwtToken, string submissionNumber)
        {
            this.Logger.LogInformation("GetTransactionTags() called in SubmissionService for submission Number: " + submissionNumber);

            var baseUrl = this.Config.GetValue<string>("SLIPAMSAPIAddress");
            var getTransactionUrl = $"{baseUrl}api/submissions/{submissionNumber}/transactions/tags";

            using (HttpClient client = new HttpClient())
            using (var formData = new MultipartFormDataContent())
            {
                // Add all required values to be sent to API.
                formData.Add(new StringContent(submissionNumber), "submissionNumber");

                // Add JWT Token received after successful authentication.
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
                
                this.Logger.LogInformation("Making an API call to GetTransactionTags with url: " + getTransactionUrl);

                //Setting up the response...
                var response = client.GetAsync(getTransactionUrl).Result;

                this.Logger.LogInformation($"Response Status Code is {response.StatusCode.ToString()}");

                if (response.IsSuccessStatusCode)
                {
                    this.Logger.LogInformation("Get Transations successfully called");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    this.Logger.LogInformation($"Response content is {responseContent}");

                    var submissionTransactions = JsonConvert.DeserializeObject<SubmissionTransactionTags>(responseContent);

                    this.Logger.LogInformation($"Submission number: {submissionTransactions.SubmissionNumber}");
                    this.Logger.LogInformation($"Number of Transactions: {submissionTransactions.Transactions.Count}");

                    var transactions = new List<TransactionDetailTags>();
                    transactions = submissionTransactions.Transactions;
                    
                    foreach (var transaction in transactions)
                    {
                        this.Logger.LogInformation("******************************");
                        this.Logger.LogInformation($"Transaction ID: {transaction.TransactionID}");
                        this.Logger.LogInformation($"Policy Number: {transaction.PolicyNumber}");
                        this.Logger.LogInformation($"Transaction Type: {transaction.TransactionType}");
                        this.Logger.LogInformation($"Premium: ${transaction.Premium}");
                        this.Logger.LogInformation($"Taxable Fees: ${transaction.TaxableFees}");
                        this.Logger.LogInformation($"Insured Name: {transaction.InsuredName}");
                        this.Logger.LogInformation($"Endorsement Number: {transaction.EndorsementNumber}");
                        this.Logger.LogInformation($"Effective Date: {String.Format("{0:MM/dd/yyyy}", transaction.EffectiveDate)}");
                        this.Logger.LogInformation($"Status: {transaction.Status}");
                        this.Logger.LogInformation($"Number of Tags: {transaction.TagDetails?.Count ?? 0}");
                        foreach(var tag in transaction.TagDetails)
                        {
                        this.Logger.LogInformation("-----------------------------");
                            this.Logger.LogInformation($"Tag Number: {tag.TagNumber}");
                            this.Logger.LogInformation($"Tag Code: {tag.TagCode}");
                            this.Logger.LogInformation($"Category: {tag.Category}");
                        }
                        this.Logger.LogInformation("******************************");
                    }

                    return submissionTransactions;
                }
                else if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    this.Logger.LogWarning("Get Transactions error returned Unauthorized status ");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(responseContent);

                    foreach (var error in errorResponse.Errors)
                    {
                        this.Logger.LogWarning($"Get Transactions error: {error}");
                    }

                    return null;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    this.Logger.LogWarning("Get Transactions error returned InternalServerError status  ");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    var error = JsonConvert.DeserializeObject<dynamic>(responseContent);

                    this.Logger.LogWarning($"{error}");

                    return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task<string> CreateNewSubmission(string jwtToken, string jsonFilePath, string zipFilePath)
        {
            this.Logger.LogInformation("CreateNewSubmission() called in SubmissionService with following json file path: " + jsonFilePath);

            var baseUrl = this.Config.GetValue<string>("SLIPAMSAPIAddress");
            var createSubmissionUrl = string.Format(baseUrl + "api/submissions");
            var json = string.Empty;

            if (File.Exists(jsonFilePath))
            {
                using (StreamReader r = new StreamReader(jsonFilePath))
                {
                    json = r.ReadToEnd();
                }
            }
            else
            {
                this.Logger.LogWarning($"json File does not exists at {jsonFilePath}");
                return null;
            }

            if (!File.Exists(zipFilePath))
            {
                this.Logger.LogWarning($"Zip File does not exists at {zipFilePath}");
                return null;
            }

            byte[] zipFileBytes = System.IO.File.ReadAllBytes(zipFilePath);
            HttpContent zipBytesContent = new ByteArrayContent(zipFileBytes);

            // Submit the form using HttpClient and 
            // create form data as Multipart (enctype="multipart/form-data")
            using (var client = new System.Net.Http.HttpClient())
            using (var formData = new MultipartFormDataContent())
            {
                // Add all required values to be sent to API.
                formData.Add(new StringContent(json), "json");
                formData.Add(zipBytesContent, "file ", Path.GetFileName(zipFilePath));
                formData.Add(new StringContent("false"), "previewInSlip");
                formData.Add(new StringContent("Submitted using SLIP AMS Example Client"), "comments");

                // Add JWT Token received after successful authentication.
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);

                // Actually invoke the request to the server
                this.Logger.LogInformation("Making an API call to CreateSubmission with Url: " + createSubmissionUrl);
                var response = client.PostAsync(createSubmissionUrl, formData).Result;

                this.Logger.LogInformation($"Response Status Code is {response.StatusCode.ToString()}");

                if (response.IsSuccessStatusCode)
                {
                    this.Logger.LogInformation("Create Submission successful");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    this.Logger.LogInformation($"Response content is {responseContent}");

                    var responseContentObject = JsonConvert.DeserializeObject<dynamic>(responseContent);
                    return responseContentObject.SubmissionNumber;
                }
                else if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    this.Logger.LogWarning("Create submission error: Invalid or missing JWT bearer token.");
                    return null;
                }
                else if (response.StatusCode == HttpStatusCode.UnsupportedMediaType)
                {
                    this.Logger.LogWarning("Create submission error: Form enctype not set to multipart/form-data.");
                    return null;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    this.Logger.LogWarning("Create submission error: Invalid submission which has following errors.");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(responseContent);

                    foreach (var error in errorResponse.Errors)
                    {
                        this.Logger.LogWarning($"Create submission error: {error}");
                    }

                    return null;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    this.Logger.LogWarning("Create submission error: Internal Server Error");

                    var responseContent = await response.Content.ReadAsStringAsync();
                    this.Logger.LogWarning(responseContent);

                    return null;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
