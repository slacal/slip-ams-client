﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SLACAL.SLIP.AMS.Core.Interfaces;

namespace SLACAL.SLIP.AMS.Infrastructure
{
    public class AuthenticateService : IAuthenticateService
    {
        public AuthenticateService(IConfiguration config, ILogger<AuthenticateService> logger)
        {
            this.Config = config;
            this.Logger = logger;
        }

        private IConfiguration Config { get; }

        private ILogger<AuthenticateService> Logger { get; }

        /// <summary>
        /// This method will authenticate using the credentials stored in appsettings file.
        /// </summary>
        /// <returns>Returns a valid JWT token if authentication is successful and null if authentication failed.</returns>
        public async Task<string> Authenticate()
        {
            // get the authentication credetials from appsettings
            (var slaBrokerNumber, var userName, var apiKey) = this.LoadCredentials();

            // get the base API URL from appsettings
            var baseUrl = this.Config.GetValue<string>("SLIPAMSAPIAddress");

            // create the full authentication URL
            var authenticationUrl = $"{baseUrl}api/sessions?SLABrokerNumber={slaBrokerNumber}&Username={userName}&APIKey={apiKey}";

            this.Logger.LogInformation($"Making authenticate API call to {authenticationUrl}");

            using var client = new HttpClient();

            // send the request
            try
            {
                var response = await client.PostAsync(authenticationUrl, null);

                this.Logger.LogInformation($"Response status code is {response.StatusCode}");

                if (response.IsSuccessStatusCode)
                {
                    // authentication successful
                    var responseContent = await response.Content.ReadAsStringAsync();

                    this.Logger.LogInformation($"Response content is {responseContent}");

                    var responseContentObject = JsonConvert.DeserializeObject<dynamic>(responseContent);

                    return responseContentObject.Token;
                }
                else
                {
                    // authentication failed
                    return null;
                }
            }
            catch (Exception ex)
            {
                this.Logger.LogInformation(ex.Message);
                return null;
            }
            
        }

        /// <summary>
        /// Load the credentials stored in appsettings file.
        /// </summary>
        /// <returns>The credentials.</returns>
        private (string slaBrokerNumber, string userName, string apiKey) LoadCredentials()
        {
            (var slaBrokerNumber, var userName, var apiKey) = (
                this.Config.GetValue<string>("AuthenticationCredentials:SLABrokerNumber"),
                this.Config.GetValue<string>("AuthenticationCredentials:UserName"),
                this.Config.GetValue<string>("AuthenticationCredentials:APIKey")
            );

            if (string.IsNullOrEmpty(slaBrokerNumber) || string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(apiKey))
            {
                this.Logger.LogCritical("Credentials must be specified in the appsettings file");

                throw new Exception("Credentials must be specified in the appsettings file");
            }

            return (
                slaBrokerNumber,
                userName,
                apiKey
            );
        }
    }
}
